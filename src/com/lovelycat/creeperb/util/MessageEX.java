package com.lovelycat.creeperb.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MessageEX {
    private Variable v = new Variable();
    public void public_(String s) {
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&',s));
    }
    public void private_(Player player, String s) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&',s));
    }
    public void private_(CommandSender commandSender, String s) {
        commandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',s));
    }
    public void ppublic_(String s) {
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+s));
    }
    public void private_(ConsoleCommandSender consoleCommandSender, String s) {
        consoleCommandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',s));
    }
    public void pprivate_(Player player, String s) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+s));
    }
    public void pprivate_(CommandSender commandSender, String s) {
        commandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+s));
    }
    public void pprivate_(ConsoleCommandSender consoleCommandSender, String s) {
        consoleCommandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+s));
    }
    public void pprivate_perm(Player player, String s) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+v.noperm+"<"+s+">"));
    }
    public void pprivate_perm(CommandSender commandSender, String s) {
        commandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+v.noperm+"<"+s+">"));
    }
    public void pprivate_perm(ConsoleCommandSender consoleCommandSender, String s) {
        consoleCommandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',v.prefix+v.noperm+"<"+s+">"));
    }

}

package com.lovelycat.creeperb.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class Http {
    public static String Http_sendGetRequest(String GETurl){
        StringBuilder sb=new StringBuilder();
        try {
            URL url=new URL(GETurl);
            InputStream in=new BufferedInputStream(url.openStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
            String line;
            while((line=reader.readLine())!=null){
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}

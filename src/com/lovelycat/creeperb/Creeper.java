package com.lovelycat.creeperb;

import com.lovelycat.creeperb.util.MessageEX;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import static com.lovelycat.creeperb.PlayerCreeperEvent.getSong;

public class Creeper extends JavaPlugin {
    private String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    private MessageEX m = new MessageEX();
    private ConsoleCommandSender ccs = getServer().getConsoleSender();
    private boolean dodisable = false;
    public static boolean isStart = false;
    public static String up = null;
    @Override
    public void onEnable() {
        m.private_(ccs,"&a√ 已成功兼容当前服务器版本 &6>> &b"+version);
        Bukkit.getPluginManager().registerEvents(new PlayerCreeperEvent(), this);
        m.pprivate_(ccs,"&b你现在可以开始&aCreeper？&b啦！");
        if (dodisable) {
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String cmd = command.getName();
        if (cmd.equalsIgnoreCase("creeper") || cmd.equalsIgnoreCase("crp")) {
            if (!sender.hasPermission("creeperbot.use")) {
                m.pprivate_perm(sender,"creeperbot.use");
                return true;
            }
            switch (args.length) {
                case 0:
                    m.pprivate_(sender,"&a/crp start &6开始creeper?！ \n &a/crp stop &c停止creeper? \n &d开始后请输入c#歌词开始 \n &d例：c#aww man");
                    break;
                case 1:
                    if (args[0].equalsIgnoreCase("start")) {
                        if (!sender.hasPermission("creeperbot.start")) {
                            m.pprivate_perm(sender,"creeperbot.start");
                            return true;
                        }
                        if (!sender.hasPermission("creeperbot.use")) {
                            m.pprivate_perm(sender,"creeperbot.use");
                            return true;
                        }
                        isStart = true;
                        m.ppublic_("creeper?");
                        up = "creeper?";
                    }else if (args[0].equalsIgnoreCase("stop")) {
                        if (!sender.hasPermission("creeperbot.stop")) {
                            m.pprivate_perm(sender,"creeperbot.stop");
                            isStart = false;
                            return true;
                        }
                    }
                    break;
            }
        }
        return true;
    }
}

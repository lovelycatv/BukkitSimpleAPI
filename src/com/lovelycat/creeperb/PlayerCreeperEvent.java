package com.lovelycat.creeperb;

import com.lovelycat.creeperb.util.MessageEX;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PlayerCreeperEvent implements Listener {
    private MessageEX m = new MessageEX();
    @EventHandler
    public void onCreeper(AsyncPlayerChatEvent event) {
        String creeper = event.getMessage();
        String next = "";
        if (Creeper.isStart) {
            if (creeper.contains("c#")) {
                if (event.getPlayer().hasPermission("creeperbot.next")) {
                    event.setCancelled(true);
                    String this_ = creeper.split("#")[1];
                    m.ppublic_("&b[&r"+event.getPlayer().getName()+"&b] &6>> &r"+this_);
                    List<String> list = getSong();
                    //首先判断这一句是什么
                    float max = 0;
                    int code = 0;
                    String similar = "";
                    for (int i=0;i<list.size();i++) {
                        if (getSimilarityRatio(this_,list.get(i))>max) {
                            max = getSimilarityRatio(this_,list.get(i));
                            similar = list.get(i);
                            code = i;
                        }
                    }
                    //System.out.println("本句是："+similar);
                    //在判断这一句是否是上一句
                    if (getSimilarityRatio(Creeper.up,similar)>0.5|similar.equalsIgnoreCase(Creeper.up)) {
                        m.ppublic_("&c请接下一句！");
                    }else {
                        //如果不是则获取应该接的句子
                        next = list.get(code);
                        //System.out.println("N下一句："+next);
                        if (getSimilarityRatio(next,similar)>0.5|similar.equalsIgnoreCase(next)) {
                            Creeper.up = list.get(code+1);
                            m.ppublic_("&b"+Creeper.up);
                            //机器人说完啦 那么更新上一句
                            Creeper.up = list.get(code+1);
                        }else {
                            m.ppublic_("&c不要乱接啦！");
                        }
                    }
                }else {
                    m.pprivate_(event.getPlayer(),"&c你不能接下去，因为你没有权限！");
                }
            }
        }
    }
    private int compare(String str, String target) {
        int d[][];              // 矩阵
        int n = str.length();
        int m = target.length();
        int i;                  // 遍历str的
        int j;                  // 遍历target的
        char ch1;               // str的
        char ch2;               // target的
        int temp;               // 记录相同字符,在某个矩阵位置值的增量,不是0就是1
        if (n == 0) { return m; }
        if (m == 0) { return n; }
        d = new int[n + 1][m + 1];
        for (i = 0; i <= n; i++)
        {                       // 初始化第一列
            d[i][0] = i;
        }

        for (j = 0; j <= m; j++)
        {                       // 初始化第一行
            d[0][j] = j;
        }

        for (i = 1; i <= n; i++)
        {                       // 遍历str
            ch1 = str.charAt(i - 1);
            // 去匹配target
            for (j = 1; j <= m; j++)
            {
                ch2 = target.charAt(j - 1);
                if (ch1 == ch2 || ch1 == ch2+32 || ch1+32 == ch2)
                {
                    temp = 0;
                } else
                {
                    temp = 1;
                }
                // 左边+1,上边+1, 左上角+temp取最小
                d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + temp);
            }
        }
        return d[n][m];
    }

    private int min(int one, int two, int three) {
        return (one = one < two ? one : two) < three ? one : three;
    }

    /**
     * 获取两字符串的相似度
     */

    public float getSimilarityRatio(String str, String target) {
        return 1 - (float) compare(str, target) / Math.max(str.length(), target.length());
    }
    public static List<String> getSong(){
        List<String> list = new ArrayList<>();
        String[] s = readTxt("plugins/CreeperBot/song.txt").split("#");
        for (int i=0;i<s.length;i++) {
            list.add(s[i]);
        }
        return list;
    }
    public static String readTxt(String filePath) {
        String res = null;
        try {
            File file = new File(filePath);
            if (file.isFile() && file.exists()) {
                InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "utf-8");
                BufferedReader br = new BufferedReader(isr);
                String lineTxt = null;
                while ((lineTxt = br.readLine()) != null) {
                    res = res + lineTxt;
                }
                br.close();
            } else {
                System.out.println("文件不存在!");
            }
        } catch (Exception e) {
            System.out.println("文件读取错误!");
        }
        return res;
    }
}
